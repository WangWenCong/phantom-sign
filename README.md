# 幻影签

#### 介绍

使用 electron + vue + vue-cli 实现的桌面便签软件,模仿了敬业签,只实现了一部分功能

#### 安装包

- 蓝奏云

[安装包版 https://wwa.lanzoui.com/iDWb6po2ccf](https://wwa.lanzoui.com/iDWb6po2ccf)

[免安装版 https://wwa.lanzoui.com/iY56vpo2cx](https://wwa.lanzoui.com/iY56vpo2cxg)

#### 截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/200520_05f5a119_5198475.png 'Snipaste_2021-02-19_20-04-27.png')
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/200527_5ef4c572_5198475.png 'Snipaste_2021-02-19_20-04-31.png')
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/200604_12750227_5198475.png 'Snipaste_2021-02-19_20-04-35.png')
![输入图片说明](https://images.gitee.com/uploads/images/2021/0219/200807_e2eed497_5198475.png 'Snipaste_2021-02-19_20-07-39.png')
![编辑窗口](https://images.gitee.com/uploads/images/2021/0219/195646_998042d5_5198475.png 'Snipaste_2021-02-19_19-53-27.png')

