const path = require('path');
// shell: 模块提供了集成其他桌面客户端的关联功能
// dialog 模块提供了api来展示原生的系统对话框，例如打开文件框，alert框，所以web应用可以给用户带来跟系统应用相同的体验.
const { ipcRenderer, shell, remote } = require('electron');
const { dialog } = remote;

window.electron = {
  ipcRenderer: {
    send: (channel, data) => {
      // whitelist channels
      let validChannels = [
        'toMain',
        'mainWindow',
        'suspensionWindow',
        'editorWindow',
        'window-move-open-mainWindow',
        'window-move-open-suspensionWindow',
      ];
      if (validChannels.includes(channel)) {
        ipcRenderer.send(channel, data);
      }
    },
    on: (channel, func) => {
      let validChannels = ['fromMain', 'message', 'font'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => {
          func(...args);
        });
      }
    },
  },
  shell: {
    openUrl: (url) => {
      shell.openExternal(url);
    },
    showItemInFolder: (filePath) => {
      shell.showItemInFolder(path.join(filePath));
    },
    openPath: (filePath) => {
      shell.openPath(path.join(filePath));
    },
  },
  dialog: {
    showOpenDialog: (options) => dialog.showOpenDialog(options)
  }
};
