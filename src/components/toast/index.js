import Toast from "./Toast";
const toast = {
  install(Vue) {
    // 创建组件构造器
    const constructorToast = Vue.extend(Toast);
    // 根据构造器,创建出来一个组件对象
    const toast = new constructorToast();
    // 将组件对象,手动挂载到某一元素上
    toast.$mount(document.createElement("div"));
    document.body.appendChild(toast.$el);
    // vue对象加属性
    Vue.prototype.$toast = toast;
  }
};

export default toast;
