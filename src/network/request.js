//封装网络情求
import axios from 'axios';

export function request(config) {
  //创建axios实例
  const instance = axios.create({
    // baseURL: "http://localhost:3000",
    timeout: 4000,
  });

  //拦截器
  //请求拦截
  instance.interceptors.request.use(
    (config) => {
      //请求成功的拦截函数
      // console.log("请求成功", config);
      //作用:
      //1.拦截不符合要求的数据或加入一些数据(比如:消息头信息)
      //2.在发送网络请求后,显示一个加载中的动画
      //3.某些请求要求用户必须登录,判断用户是否有token,如果没有token跳转到login页面
      return config; //拦截后必须返回去,否则会请求失败
    },
    (err) => {
      //请求失败的拦截函数
      //请求拦截中错误拦截较少，通常都是配置相关的拦截可能的错误比如请求超时，
      //可以将页面跳转到一个错误页面中
      console.log('请求失败', err);
    }
  );
  //响应拦截
  instance.interceptors.response.use(
    (config) => {
      //响应成功的拦截函数
      // console.log("响应成功", config);
      //响应的成功拦截中，主要是对数据进行过滤
      return config; //必须返回,否则instance(config)函数不会接收到数据
    },
    (err) => {
      //响应失败的拦截函数
      //响应的失败拦截中，可以根据status判断报错的错误码，跳转到不同的错误提示页面。
      console.log('响应失败', err);
    }
  );

  return instance(config);
}
