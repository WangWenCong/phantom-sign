import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import VueQuillEditor from 'vue-quill-editor';

import db from 'common/datastore.js';
// 写一个模态框插件,通过代码调用即可
import toast from 'components/toast';

Vue.config.productionTip = false;

Vue.use(VueQuillEditor);

Vue.use(db);

Vue.use(toast);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
